package com.jwt.model;

public class Input {

	private int id;
	private String input_value;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInput_value() {
		return input_value;
	}
	public void setInput_value(String input_value) {
		this.input_value = input_value;
	}
}
