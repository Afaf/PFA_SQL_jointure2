package com.jwt.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Semester {
	
	@Id
	private int id;
	
	@Column
	private int num_semester;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_subject", nullable = false)
	private Subject subject ;
	
	
	public int getId_subject() {
		return subject.getId_subject();
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNum_semester() {
		return num_semester;
	}

	public void setNum_semester(int num_semester) {
		this.num_semester = num_semester;
	}


	
	

}
