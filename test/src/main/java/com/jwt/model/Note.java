package com.jwt.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Note {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column
	private int value;
	
	// Note/Student mapping
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_student", nullable = false)
	private Student student ;
	
	public int getId_student() {
		return this.student.getId_student();
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	// Note/Subject mapping
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_subject", nullable = false)

	private Subject subject ;
	
	
	public int getId_subject() {
		return subject.getId_subject();
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}


}
