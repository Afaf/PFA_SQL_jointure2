package com.jwt.controller;


import java.util.List;

import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jwt.model.Input;
import com.jwt.model.Note;
import com.jwt.model.ResultNote;
import com.jwt.model.Semester;
import com.jwt.model.Student;
import com.jwt.model.Subject;

import com.jwt.service.EvaluationService;





@Controller
public class EvaluationController{
		
	String message_right;
	String message_wrong;
	List<ResultNote> rnotes;
	
	@Autowired
	private EvaluationService eservice;

	@RequestMapping(value = "/")
	public String form(Model model)
	{
		Input input=new Input();
		model.addAttribute("input",input);
		model.addAttribute("message_right",message_right);
		model.addAttribute("message_wrong",message_wrong);
		List<Student> students = this.eservice.getStudentsList();
		List<Subject> subjects = this.eservice.getSubjectsList();
		List<Note> notes=this.eservice.getNotesList();
		List<Semester> semesters = this.eservice.getSemestersList();
		model.addAttribute("students",students);
		model.addAttribute("subjects",subjects);
		model.addAttribute("notes",notes);
		model.addAttribute("semesters",semesters);
		
		/*List<ResultNote> rnotes=eservice.getResultNotes();
		model.addAttribute("rnotes",rnotes);*/
		return "home";
		
	}
	
	@RequestMapping(value = "/send" ,method = RequestMethod.POST)
	public String resgistration(Model model, @ModelAttribute("input") Input input)
	{
		
		 /*rnotes=eservice.getResultNotes(input.getInput_value());*/
		
		try {
		if (this.eservice.verifyQuery(input.getInput_value()))
			{
			message_wrong=null;
			message_right = "the answer is correct";
			}
		else
		{
			message_right=null;
			message_wrong = "the answer is wrong"; 
			}
		}catch(SQLGrammarException e){
			message_right=null;
			message_wrong = "the answer is wrong"; 
		}
		return "redirect:/";
	}
	
	
}
