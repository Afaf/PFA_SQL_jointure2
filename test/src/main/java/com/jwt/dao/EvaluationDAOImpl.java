package com.jwt.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Note;
import com.jwt.model.ResultNote;
import com.jwt.model.Semester;
import com.jwt.model.Student;
import com.jwt.model.Subject;

@Repository
public class EvaluationDAOImpl implements EvaluationDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Student> getStudentsList() {

		return sessionFactory.getCurrentSession().createQuery("from Student").list();

	}

	public List<Subject> getSubjectsList() {
		
		return sessionFactory.getCurrentSession().createQuery("from Subject").list();

	}

	public List<Note> getNotesList() {
		
		return sessionFactory.getCurrentSession().createQuery("from Note").list();

	}

	public List<Semester> getSemestersList() {
		
		return sessionFactory.getCurrentSession().createQuery("from Semester").list();

	}

	public List<ResultNote> getResultNotes() {
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("select student.name , subject.title,note.value from student , subject , note where student.id_student=note.id_student and subject.id_subject=note.id_subject and note.value in (select max(note.value) from note , semester where note.id_subject= semester.id_subject group by note.id_subject , semester.num_semester )");
		List<Object[]>	rows = query.list();
		List<ResultNote> resultNotes = new ArrayList<ResultNote>();
			for(Object[] row : rows){
				ResultNote rnote = new ResultNote();
				rnote.setName(row[0].toString());
				rnote.setSubject(row[1].toString());
				rnote.setNote(Integer.parseInt(row[2].toString()));
				resultNotes.add(rnote);
			}
		
			return resultNotes;
			
	}

	public List<ResultNote> getResultNotes(String requete) {
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(requete);
		List<Object[]>	rows = query.list();
		List<ResultNote> resultNotes = new ArrayList<ResultNote>();
			for(Object[] row : rows){
				ResultNote rnote = new ResultNote();
				rnote.setName(row[0].toString());
				rnote.setSubject(row[1].toString());
				rnote.setNote(Integer.parseInt(row[2].toString()));
				resultNotes.add(rnote);
			}
		
			return resultNotes;

	}

	
	
}
