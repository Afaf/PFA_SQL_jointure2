package com.jwt.service;

import java.util.List;

import com.jwt.model.Note;
import com.jwt.model.ResultNote;
import com.jwt.model.Semester;
import com.jwt.model.Student;
import com.jwt.model.Subject;

public interface EvaluationService {

	
	public List<Student> getStudentsList(); 
	public List<Subject> getSubjectsList(); 
	public List<Note> getNotesList(); 
	public List<Semester> getSemestersList(); 
	public List<ResultNote> getResultNotes();
	public List<ResultNote> getResultNotes(String requete);
	public boolean verifyQuery(String query);

}
