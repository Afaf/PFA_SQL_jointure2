package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.EvaluationDAO;
import com.jwt.model.Note;
import com.jwt.model.ResultNote;
import com.jwt.model.Semester;
import com.jwt.model.Student;
import com.jwt.model.Subject;

@Service
@Transactional
public class EvaluationServiceImpl implements EvaluationService{

	
	@Autowired
	private EvaluationDAO edao;
	
	public List<Student> getStudentsList() {
		
		return edao.getStudentsList();
		
	}

	public List<Subject> getSubjectsList() {
		
		return edao.getSubjectsList();
		
	}

	public List<Note> getNotesList() {

		return edao.getNotesList();
		
	}

	public List<Semester> getSemestersList() {

		return edao.getSemestersList();
				
	}

	public List<ResultNote> getResultNotes() {

		return edao.getResultNotes();
	}

	public List<ResultNote> getResultNotes(String requete) {
		return edao.getResultNotes(requete);
	}

	public boolean verifyQuery(String requete) {
		List<ResultNote> list1 = this.getResultNotes();
		List<ResultNote> list2 = this.getResultNotes(requete);
		
		for(int i=0;i<list1.size();i++){
			if(!(list1.get(i).getName().equals(list2.get(i).getName()))
				|| 	!(list1.get(i).getSubject().equals(list2.get(i).getSubject()))
				|| 	!(list1.get(i).getNote()==(list2.get(i).getNote()))
					)
				return false ;
		}
		return true;
		
	}
	
}


