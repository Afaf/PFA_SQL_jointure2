<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"  />
<title>SQL Evaluation</title>
<style type="text/css">
.espace{
margin-left:2em;
}
.title{
color:red;
}
</style>
</head>
<body>
	
	<div class="container">
		<div class="jumbotron">
			<h1>SQL Evaluation</h1>
			<p>This is a SQL Evaluation </p>
		</div>
		<h2> return list of students having the best score in every subject per semester </h2>            
 	</div>
 	
  <div class="container">
 	 <div class="row">
 	 
 		 <div class="col-sm-3">
			<h3 class="title"> Student table </h3>
    		<c:forEach var="student" items="${students}">
      			<p>< student id_student="${student.id_student}"></p>
      			<p class="espace">< name>${student.name}< /name></p>
        		<p class="espace">< CNE>${student.CNE}< /CNE></p>
      			<p>< /student> </p>
     		</c:forEach>
    	 </div>
	
	 	 <div class="col-sm-3">
			<h3 class="title"> Subject table </h3>
			<c:forEach var="subject" items="${subjects}">
      			<p>< subject id_subject="${subject.id_subject}"></p>
      			<p class="espace">< title>${subject.title}< /title></p>
        		<p class="espace">< responsable>${subject.responsable}< /responsable></p>
     			<p>< /subject></p>
     		</c:forEach>
     	</div>	
     
      	<div class="col-sm-3">
     		<h3 class="title"> Note table </h3>
     		<c:forEach var="note" items="${notes}">
      			<p>< note id_student="${note.id_student}" id_subject="${note.id_subject}"></p>
      			<p class="espace">< value>${note.value}< /value></p>
      			<p>< /note></p>
     		</c:forEach>
     	</div>
     
      	<div class="col-sm-3">
     		<h3 class="title"> Semester table </h3>
     		<c:forEach var="semester" items="${semesters}">
      			<p>< semester id_subject="${semester.id_subject}" num_semester="${semester.num_semester}" / ></p>
     		</c:forEach>
     	</div>
     
     </div>
	</div>
	
	<div class="container">
		<form:form action="send" method="POST" commandName="input">
			<div class="form-group">
				<label for="comment">your Answer : </label>
				<form:textarea path="input_value" class="form-control" rows="5"
					id="comment"></form:textarea>
				<br/>
				<button type="submit">OK</button>
			</div>
		</form:form>
	</div>
	
	
	<div class="conatiner">
	<div class="row">
        <div class="col-sm-2">
		<span class="label label-success">${ message_right }</span>
		<span class="label label-danger">${ message_wrong  }</span>
		</div>
		</div>
	</div>
	
	<!--<h3 class="title"> resut Notes</h3>
     		<c:forEach var="rnote" items="${rnotes}">
      			
      			<p class="espace">< name>${rnote.name}< /name></p>
        		<p class="espace">< note>${rnote.note}< /note></p>
     		</c:forEach>  -->
	 
</body>
</html>